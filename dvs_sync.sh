#!/bin/bash

# Meta
HOST=ops                                # Name of the docker host for notifications and filename
VERSION=latest                          # You can set this to "latest" or a specific date like "mm-dd-yyyy"

# Minio S3
MINIO_SERVER=http://dc0.spakl.io:9000   # Minio api url
BUCKET_NAME=dvs                         # Bucketname
DVS_ACCESS_KEY=d_volume_sync            # minio access key
DVS_SECRET_KEY=                         # minio secret key

# Reports
REPORT_ENABLED=true                     # Set to false to disable the report
GOTIFY_URL="http://10.10.4.10:8090"     # Gotify base url
GOTIFY_APP_TOKEN=""                     # gotify api token

# Function to log messages
log() {
    echo "[$(date '+%Y-%m-%d %H:%M:%S')]: $1"
}

# Validate if mc is installed or not
if ! command -v mc &> /dev/null; then
    log "ERROR: MinIO Client (mc) is not installed."
    exit 1
fi

# Configure MinIO server
mc alias set minio "$MINIO_SERVER" "$DVS_ACCESS_KEY" "$DVS_SECRET_KEY" --api S3v4

# Determine the backup file to restore
if [ "$VERSION" == "latest" ]; then
    backup_filename="${HOST}_volume_backup-latest.tar.gz"
else
    # Validate date format if a specific date is provided
    if ! date -d $VERSION &>/dev/null; then
        log "ERROR: Invalid date format. Please provide thvolumes/runner-xx5uddnbk-project-49097184-con
# Sync the backup from the MinIO bucket
if mc cp "minio/$BUCKET_NAME/$backup_filename" "./"; then
    log "Backup file $backup_filename downloaded successfully."
else
    log "ERROR: Failed to download the backup file $backup_filename."
    exit 1
fi

# Extract the tar.gz file to restore Docker volumes
if sudo tar -xzvf "./$backup_filename" -C /var/lib/docker; then
    log "Docker volumes restored successfully from $backup_filename."
else
    log "ERROR: Failed to restore Docker volumes."
    exit 1
fi

# Send notification to Gotify after restore
if [ "$REPORT_ENABLED" = true ] ; then
    log "Sending notification to Gotify server..."

    if sudo tar -tzf "./$backup_filename" > /dev/null 2>&1; then
        STATUS="Restore was successful"
    else
        STATUS="Restore failed"
    fi

    curl -k -X POST "$GOTIFY_URL/message?token=$GOTIFY_APP_TOKEN" \
        -F "title=Restore Status for $HOST" \
        -F "message=Restore completed on $(date '+%Y-%m-%d %H:%M:%S'). Status: $STATUS" \
        -F "priority=5"

    log "Notification sent."
fi


# Optionally, clean up the downloaded backup file
if sudo rm "./$backup_filename"; then
    log "Local backup file removed successfully."
else
    log "ERROR: Failed to remove local backup file."
fi

log "Restore process completed."
