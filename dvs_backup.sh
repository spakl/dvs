#!/bin/bash

# Meta
HOST=ops                                # Name of the docker host for notifications and filename

# Minio S3
MINIO_SERVER=http://dc0.spakl.io:9000   # Minio api url
BUCKET_NAME=dvs                         # Bucketname
DVS_ACCESS_KEY=d_volume_sync            # minio access key
DVS_SECRET_KEY=                         # minio secret key

# Reports
REPORT_ENABLED=true                     # Set to false to disable the report
GOTIFY_URL="http://10.10.4.10:8090"     # Gotify base url
GOTIFY_APP_TOKEN=""                     # gotify api token
# Function to log messages
log() {
    echo "[$(date '+%Y-%m-%d %H:%M:%S')]: $1"
}

# Validate if the necessary tools are installed or not
if ! command -v tar &> /dev/null || ! command -v curl &> /dev/null; then
    log "ERROR: The required tools (tar, curl) are not installed."
    exit 1
fi

# Get Current Date
current_date=$(date '+%m-%d-%Y')

# Create a tar.gz file of the Docker volumes with the current date appended to the filename
backup_filename="${HOST}_volume_backup-$current_date.tar.gz"
latest_backup_filename="${HOST}_volume_backup-latest.tar.gz"

if sudo tar -czvf "./$backup_filename" -C /var/lib/docker volumes; then
    log "Backup created successfully."
else
    log "ERROR: Failed to create backup."
    exit 1
fi

# Also create a 'latest' backup
sudo cp "./$backup_filename" "./$latest_backup_filename"

# Install MinIO Client if it is not already installed
if ! command -v mc &> /dev/null; then
    log "Installing MinIO Client..."
    curl -O https://dl.min.io/client/mc/release/linux-amd64/mc
    chmod +x mc
    sudo mv mc /usr/local/bin
fi

# Configure MinIO server
mc alias set minio "$MINIO_SERVER" "$DVS_ACCESS_KEY" "$DVS_SECRET_KEY" --api S3v4

# Check if the bucket exists
if mc ls minio | grep -q "$BUCKET_NAME"; then
    log "Bucket already exists."
else
    if mc mb "minio/$BUCKET_NAME"; then
        log "Bucket created successfully."
    else
        log "ERROR: Failed to create bucket."
        exit 1
    fi
fi

# Copy the tar.gz file with the current date to the bucket
if mc cp "./$backup_filename" "minio/$BUCKET_NAME"; then
    log "Backup uploaded successfully."
else
    log "ERROR: Failed to upload backup."
    exit 1
fi

# Overwrite the 'latest' backup in the bucket
if mc cp "./$latest_backup_filename" "minio/$BUCKET_NAME"; then
    log "'Latest' backup overwritten successfully."
else
    log "ERROR: Failed to overwrite 'latest' backup."
    exit 1
fi


# Send notification to Gotify after backup
if [ "$REPORT_ENABLED" = true ] ; then
    log "Sending notification to Gotify server..."

    if [[ -f "./$backup_filename" ]]; then
        STATUS="Backup was successful"
    else
        STATUS="Backup failed"
    fi

    curl -k -X POST "$GOTIFY_URL/message?token=$GOTIFY_APP_TOKEN" \
        -F "title=Backup Status for $HOST" \
        -F "message=Backup completed on $(date '+%Y-%m-%d %H:%M:%S'). Status: $STATUS" \
        -F "priority=5"

    log "Notification sent."
fi

# Clean up local backup files
if sudo rm "./$backup_filename" && sudo rm "./$latest_backup_filename"; then
    log "Local backup files removed successfully."
else
    log "ERROR: Failed to remove local backup files."
fi

log "Backup process completed."
