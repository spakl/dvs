# Docker Volumes Backup and Restore

This repository contains scripts for backing up and restoring Docker volumes. The `dvs_backup.sh` script creates a backup of your Docker volumes, and the `dvs_sync.sh` script helps in restoring them. Backups are stored on a MinIO server.

## Backup Script (dvs_backup.sh)

### Description

The backup script performs the following actions:
- Creates a backup of Docker volumes located at `/var/lib/docker/volumes`.
- Stores the backup locally with a timestamp and as the latest version.
- Uploads the backups to a specified MinIO server bucket.
- Optionally cleans up the local backup files after uploading to the MinIO server.

### Usage

1. Set your `HOST`, `DVS_ACCESS_KEY`, `DVS_SECRET_KEY`, MinIO server URL, and bucket name at the beginning of the script.
2. Make the script executable: `chmod +x dvs_backup.sh`.
3. Run the script: `./dvs_backup.sh`.

## Restore Script (dvs_sync.sh)

### Description
The restore script performs the following actions:
- Downloads the specified version of the Docker volumes backup from the MinIO server.
- Extracts the backup file to restore the Docker volumes.
- **Stop containers befor running dvs_sync**

### Usage

1. Set your `HOST`, `DVS_ACCESS_KEY`, `DVS_SECRET_KEY`, MinIO server URL, bucket name, and version (date or "latest") at the beginning of the script.
2. Make the script executable: `chmod +x dvs_sync.sh`.
3. Run the script: `./dvs_sync.sh`.



## Automating Backups with Cron

You can set up a cron job to automate the backup process. Follow these steps to create a cron job that runs the backup script at a specified interval.

### Steps

1. Open the crontab configuration file: `crontab -e`.
2. Add a new line with the following format to schedule the backup script. This example runs the backup every day at 3 a.m.:

    ```sh
    0 3 * * * /path/to/backup.sh
    ```

    Modify the cron expression (`0 3 * * *`) and the script path (`/path/to/backup.sh`) according to your needs.

3. Save and exit the editor.

### Cron Job Format

The general format of a cron job is:

```sh
* * * * * command-to-be-executed
- - - - -
| | | | | 
| | | | ----- Day of the week (0 - 7) [Both 0 and 7 represent Sunday]
| | | ------- Month (1 - 12)
| | --------- Day of the month (1 - 31)
| ----------- Hour (0 - 23)
------------- Minute (0 - 59)



### Add Cron Job
Add the following line to schedule the backup script to run at 3 a.m. every Saturday:

```bash

0 3 * * 6 /path/to/dvs_backup.sh
```

Ensure to replace "/path/to/dvs_backup.sh" with the actual path to your backup script.